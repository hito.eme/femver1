!この共役勾配法CG法のコードはRIKEN2014の講義から引用
!大文字の変数は引用元から変えてないことを示す．
!
module solver
    implicit none
    real(8), allocatable :: fmat(:),kmat(:,:),R(:),X(:),P(:),Q(:)
    real(8) :: RESID
    

    contains
    subroutine cgmethod(X,RESID)
    implicit none

    real(8), allocatable :: fmat(:),kmat(:,:),R(:),X(:),P(:),Q(:)
    real(8) :: BNRM2,ALPHA,BETA,C1,DNRM2,EPS,RESID,RHO,RHO1
    integer :: i,j,k,N,iter,ITERmax,nconst

    !!決めるべき変数itermaxとEPS
    ITERmax = nconst*2
    EPS = 0.01



    do i= 1, N
        R(i)= fmat(i)
        do j= 1, N
            R(i)= R(i) - kmat(i,j)*X(j)
        enddo
    enddo
    BNRM2= 0.0D0
    do i= 1, N
        BNRM2= BNRM2 + fmat(i) **2
    enddo

    do iter= 1, ITERmax
        RHO= 0.d0
        do i= 1, N
            RHO= RHO + R(i)*R(i) 
        enddo
        if ( iter == 1 ) then
            do i= 1, N
                P(i)= R(i)
            enddo
        else
            BETA= RHO / RHO1
            do i= 1, N
                P(i)= R(i) + BETA*P(i)
            enddo
        endif
        do i= 1, N
        Q(i)= 0.d0
            do j= 1, N
                Q(i)= Q(i) + kmat(i,j)*P(j)
            enddo
        enddo

        C1= 0.d0
        do i= 1, N
            C1= C1 + P(i)*Q(i)
        enddo
        ALPHA= RHO / C1
        do i= 1, N
            X(i)= X(i) + ALPHA * P(i)
            R(i)= R(i) - ALPHA * Q(i)
        end do

        DNRM2 = 0.0
        do i= 1, N
            DNRM2= DNRM2 + R(i)**2
        enddo
        RESID= dsqrt(DNRM2/BNRM2)
        if ( RESID <= EPS) exit
        RHO1 = RHO

    end do




    end subroutine



end module
