module fmatrixmodule
    implicit none
    !real(8), allocatable :: fmat(:)
    
     
    

    contains
    subroutine inputload(fmat,nconst)
        implicit none
        real(8), allocatable :: fmat(:)
        integer :: i,j,k,nconst
        real(8) :: o,loadx,loady
        
    
        open(50, file = "inputload.dat")
        do i = 1,nconst
            read(50, *)o,loadx,loady

            j= 2*i-1
            k= 2*i
            fmat(j) = loadx
            fmat(k) = loady

        end do
        close(50)    
    write(*,*)'fmat',fmat
    end subroutine
end module

!gfortran -c fmatmodule.f90