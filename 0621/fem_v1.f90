program fem
    use inputparameter
    use kmatrixmodule
    use fmatrixmodule
    use solver
    use solverlu
    implicit none

    
    !必要ないけど仮置きの変数
    integer :: i,j,k,o,lines
    integer,allocatable :: element(:,:)
    real(8) :: dieta(4),dizeta(4),dzeta,deta,young,poisson
    real(8),allocatable :: node(:,:),point(:,:,:),xy(:,:)
    !本当に必要な変数
    integer :: dim,econst,ele,ke,l,n,m,nconst
    real(8) :: jacobiandet
    real(8),allocatable :: dmat(:,:),bmattranspose(:,:),fmat(:),eta(:),zeta(:),kemat(:,:),allkemat(:,:,:),kmat(:,:)
    real(8),allocatable :: umat(:),kemat1(:,:)
    n = 2
    m = 4
    l = 2
    dim=2
    nconst = 6  !節点数
    econst = 2  !要素数

    
    allocate(eta(4))
    allocate(zeta(4))
    allocate(jacobianinv(2,2))
    allocate(bmat(3,8))
    allocate(bmattranspose(8,3))
    allocate(dmat(3,3))
    ! !要素，要素の節点数(2次元は4),xとyが格納される
    allocate(point(econst,4,dim))
    allocate(element(nconst,4))
    allocate(xy(m,l))
    allocate(allkemat(nconst,8,8))
    allkemat=0.0d0
    allocate(kmat(nconst*2,nconst*2))
    kmat=0.0d0
    allocate(kemat(8,8))
    kemat=0.0d0
    allocate(kemat1(8,8))
    allocate(fmat(nconst*2))
    allocate(umat(nconst*2))
    
    call input(dieta,dizeta,point,nconst,econst,dim,element,young,poisson) !inputmodule
    call dmatrix(dmat,young,poisson) !kmatmodule

    do ele = 1,econst

        call xy_point(xy,point,ele,dim)  !inputmodule
    
        do  ke = 1,4 !積分点数が2の場合
            write(*,*)""
            deta  = dieta(ke)
            dzeta = dizeta(ke)

            write(*,*) 'eta=', deta
            write(*,*) 'zeta=', dzeta 
        
            call etapd(eta,dzeta)
            call zetapd(zeta,deta)
            call jacobimat(zeta,eta,jacobiandet,jacobianinv,xy,n,m,l)
            call bmatrixmake(jacobianinv,zeta,eta,bmat)
            write(*,*)'bmat='
            do i = 1, 3
                write(*,*) (bmat(i,o),o=1,8)
            end do
            call bmatrixtranspose(bmat, bmattranspose)
            write(*,*)'bmattranspose='
            do i = 1, 8
                write(*,*) (bmattranspose(i,o),o=1,3)
            end do
        
            call kematrix(bmat,bmattranspose,dmat,ke,jacobiandet,kemat1)
         !kmatの足しこみ開始
            kemat=kemat+kemat1
            
            write(*,*)"kemat="
            do i = 1, 8
                write(*,*) (kemat(i,o),o=1,8)
            end do
         !kmatの足しこみ終わり   
        end do

        !todo:ここからサブルーチン化5/17
        do i=1,8
            do j=1,8
                allkemat(ele,i,j)=kemat(i,j)
            end do
        end do 
        
        write(*,*)"allkemat"
        do i = 1, 8
                write(*,*) (allkemat(ele,i,o),o=1,8)
        end do

        
        do i= 1,4
            do j = 1,4
                kmat(2*element(ele,i)-1,2*element(ele,j)-1:2*element(ele,j))=&
                &kmat(((2*element(ele,i))-1),2*element(ele,j)-1:2*element(ele,j))+allkemat(ele,2*i-1,2*j-1:2*j)

                kmat(2*element(ele,i)  ,2*element(ele,j)-1:2*element(ele,j))=&
                &kmat((2*element(ele,i)    ),2*element(ele,j)-1:2*element(ele,j))+allkemat(ele,2*i  ,2*j-1:2*j)
                
            end do
        end do 
        !ここまで

    end do
    write(*,*)"kmat"
    do i = 1, nconst*2
        write(*,*) (kmat(i,o),o=1,nconst*2)
    end do
    call inputload(fmat,nconst)
    call dirichletinput(n)
    call dirichletcond(kmat,n,nconst)
    call cgmethod(umat,fmat,kmat,nconst)
    !call lumethod(nconst,dim,kmat,fmat,umat)
    write(*,*)'umat',umat




end program

! gfortran fem_v1.f90 inputmodule.o kmatmodule.o fmatmodule.o solver.o solverlu.o -I/usr/local/include -llapack95 -llapack -lblas