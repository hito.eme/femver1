module kmatrixmodule
!use para 
implicit none
!
!  todo:グローバル変数を減らす 5/25
    real(8), allocatable ::bmat(:,:),jacobianinv(:,:)

contains
    
    

    subroutine keijyou(kei, deta, dzeta)
        implicit none
        real(8) :: kei(4), deta, dzeta
        !kei(1)はN1(形状関数を示す)
        kei(1) = (1.0d0/4)*((1.0d0-dzeta)*(1.0d0-deta)) 
        kei(2) = (1.0d0/4)*((1.0d0+dzeta)*(1.0d0-deta))
        kei(3) = (1.0d0/4)*((1.0d0+dzeta)*(1.0d0+deta))
        kei(4) = (1.0d0/4)*((1.0d0-dzeta)*(1.0d0+deta))

    end subroutine keijyou



    subroutine etapd(eta, dzeta)
        implicit none 
        real(8) :: eta(4),dzeta 
        !eta(1)はN1をηで偏微分したものを示す．
        eta(1) = (-1.0d0/4.0d0)*(1.0d0-dzeta)
        eta(2) = (-1.0d0/4.0d0)*(1.0d0+dzeta)
        eta(3) = (1.0d0/4.0d0)*(1.0d0+dzeta)
        eta(4) = (1.0d0/4.0d0)*(1.0d0-dzeta)

    end subroutine etapd

    subroutine zetapd(zeta, deta)
        implicit none
        real(8)  :: zeta(4), deta
        !zeta(1)はN1をζで偏微分したものを示す．
        zeta(1) = (-1.0d0/4.0d0)*(1.0d0-deta)
        zeta(2) = (1.0d0/4.0d0)*(1.0d0-deta)
        zeta(3) = (1.0d0/4.0d0)*(1.0d0+deta)
        zeta(4) = (-1.0d0/4.0d0)*(1.0d0+deta)

    end subroutine zetapd

    subroutine jacobimat(zeta,eta,jacobiandet,jacobianinv,xy,n,m,l)
        implicit none
        integer :: j,n,m,l
        real(8) :: jacobian(n,m),jacobianmat(n,l),xy(m,l),jacobiandet
        real(8),allocatable :: jacobianinv(:,:), zeta(:),eta(:)
        
        
            do j = 1,m
                jacobian(1,j) = zeta(j)
                jacobian(2,j) = eta(j)
            end do
        write(*,*)'jacobian',jacobian
        jacobianmat = matmul(jacobian, xy)
        write(*,*)'jacobianmat',jacobianmat
        jacobiandet = (jacobianmat(1, 1) * jacobianmat(2, 2))-(jacobianmat(1, 2)*jacobianmat(2, 1))
        write(*,*)'jacobiandet',jacobiandet
        jacobianinv(1,1) =jacobianmat(2,2)
        jacobianinv(2,1) =-jacobianmat(2,1)
        jacobianinv(1,2) =-jacobianmat(1,2)
        jacobianinv(2,2) =jacobianmat(1,1)
        jacobianinv = (1/(jacobiandet))*(jacobianinv)
       
        
    end subroutine jacobimat

    !Todo: ラパック必要ないのでベタ打ちに変更　5/25
    subroutine bmatrixmake(jacobianinv,zeta,eta,bmat)
        
        implicit none
        real(8),save :: x(4)=0.0d0,y(4)=0.0d0
        real(8),allocatable :: jacobianinv(:,:),bmat(:,:),zeta(:),eta(:)
        integer :: k
       
       
        do k= 1,4 !四角形要素の場合
            
            x(k)=(jacobianinv(1,1)*zeta(k))+(jacobianinv(1,2)*eta(k))
            y(k)=(jacobianinv(2,1)*zeta(k))+(jacobianinv(2,2)*eta(k))
        end do
        bmat(1,1)=x(1)
        bmat(2,1)=0
        bmat(3,1)=y(1)
        bmat(1,2)=0
        bmat(2,2)=y(1)
        bmat(3,2)=x(1)

        bmat(1,3)=x(2)
        bmat(2,3)=0
        bmat(3,3)=y(2)
        bmat(1,4)=0
        bmat(2,4)=y(2)
        bmat(3,4)=x(2)

        bmat(1,5)=x(3)
        bmat(2,5)=0
        bmat(3,5)=y(3)
        bmat(1,6)=0
        bmat(2,6)=y(3)
        bmat(3,6)=x(3)

        bmat(1,7)=x(4)
        bmat(2,7)=0
        bmat(3,7)=y(4)
        bmat(1,8)=0
        bmat(2,8)=y(4)
        bmat(3,8)=x(4)
        

    end subroutine bmatrixmake



    subroutine bmatrixtranspose(bmat, bmattranspose)
    implicit none
        real(8),allocatable :: bmat(:,:),bmattranspose(:,:)
        bmattranspose = transpose(bmat)
    end subroutine bmatrixtranspose

    subroutine dmatrix(dmat,young,poisson)
        implicit none
        real(8), allocatable :: dmat(:,:)
        real(8) :: poisson,  young
        integer :: i, o
    
        dmat(1,1)=young/(1-(poisson)**2)
        dmat(2,1)=(young*poisson)/(1-(poisson)**2)
        dmat(3,1)=0

        dmat(1,2)=(young*poisson)/(1-(poisson)**2)
        dmat(2,2)=young/(1-(poisson)**2)
        dmat(3,2)=0

        dmat(1,3)=0
        dmat(2,3)=0
        dmat(3,3)=young/(2-(2*poisson))

        write(*,*)'dmat='
        do i = 1, 3
            write(*,*) (dmat(i,o),o=1,3)
        end do

    end subroutine 


    

    subroutine kematrix(bmat,bmattranspose,dmat,ke,jacobiandet,kemat1)
        real(8) :: kemat1(8,8),kemat11(8,3),kemat12(8,8)
        real(8) :: jacobiandet
        real(8),allocatable :: bmat(:,:),dmat(:,:),bmattranspose(:,:),kemat(:,:)
        integer :: i,o,ke
        kemat1=0.0d0
        kemat11=0.0d0
        kemat12=0.0d0
        
        !ここら辺が怪しい6/17変更必須
        
        kemat11 = matmul(bmattranspose,dmat)
        kemat12 = matmul(kemat11,bmat)
        kemat1 = kemat12*jacobiandet
        write(*,*)'kemat='
        do i = 1, 8
            write(*,*) (kemat1(i,o),o=1,8)
        end do

    end subroutine kematrix

end module

!コンパイル時は
        ! gfortran -c kmatmodule.f90

