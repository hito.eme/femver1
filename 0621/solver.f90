!この共役勾配法CG法のコードはRIKEN2014の講義から引用
!大文字の変数は引用元から変えてないことを示す．
!
module solver
    implicit none
    
    

contains
    subroutine dirichletinput(nb)
    implicit none
    integer :: nb
    open(60,file = "boundary.dat",status='old')
    read(60, *) nb
    
    end subroutine

    subroutine dirichletcond(kmat,nb,nconst)
    implicit none
    real(8), allocatable :: kmat(:,:)
    integer :: nb,i,j,o,nconst,coi,coj
    type bound
        real(8),allocatable ::  displacement(:)
        integer,allocatable :: conode(:),direction(:)
    end type bound
    type(bound) :: boundary
    allocate(boundary%conode(nb))
    allocate(boundary%direction(nb))
    allocate(boundary%displacement(nb))
    boundary%conode = 0
    boundary%direction = 0
    boundary%displacement = 0.0d0

    read(60,"()")
    do i=1,nb  
        read(60,*)boundary%conode(i), boundary%direction(i), boundary%displacement(i)
    end do
    i=1
    close(60)
   

    do i = 1,nb
        coj = (boundary%conode(i)*2) - (boundary%direction(i))
        
        do j=1,nconst*2
            kmat(j,coj)=0.0d0
            kmat(coj,j)=0.0d0
            kmat(coj,coj)=1.0d0
        end do
    end do
  
    write(*,*)"solverkmat"
    do i = 1, 12
            write(*,*) (kmat(i,o),o=1,12)
    end do
    end subroutine

    subroutine cgmethod(umat,fmat,kmat,nconst)
        implicit none

        

        real(8), allocatable :: fmat(:),kmat(:,:),umat(:)
        real(8) :: BNRM2,ALPHA,BETA,C1,DNRM2,EPS,RESID,RHO,RHO1,R(nconst*2),P(nconst*2),Q(nconst*2)
        real(8) :: testy(nconst*2)
        integer :: i,j,k,o,N,iter,ITERmax,nconst

        !!決めるべき変数itermaxとEPS
        !ToDO:なぜかnconst読み込まれない32867みたいになる
        
        ITERmax = nconst*2
        N = nconst*2
        EPS = 0.00001
        umat=0.0d0
 
        !Todo:ここがエラーの根幹5/24
        !
        
        do i= 1, nconst*2
            R(i)= fmat(i)
            do j= 1, nconst*2
                R(i)= R(i) - kmat(i,j)*umat(j)
            enddo
        enddo
        
        
        BNRM2= 0.0D0
        do i= 1, N
            BNRM2= BNRM2 + fmat(i) **2
        enddo

        do iter= 1, ITERmax
            RHO= 0.d0
            do i= 1, N
                RHO= RHO + R(i)*R(i) 
            enddo
            if ( iter == 1 ) then
                do i= 1, N
                    P(i)= R(i)
                enddo
            else
                BETA= RHO / RHO1
                do i= 1, N
                    P(i)= R(i) + BETA*P(i)
                enddo
            endif
            do i= 1, N
            Q(i)= 0.d0
                do j= 1, N
                    Q(i)= Q(i) + kmat(i,j)*P(j)
                enddo
            enddo
            C1= 0.d0
            do i= 1, N
                C1= C1 + P(i)*Q(i)
            enddo
            
            ALPHA= RHO / C1
            do i= 1, N
                umat(i)= umat(i) + ALPHA * P(i)
                R(i)= R(i) - ALPHA * Q(i)
            end do
            DNRM2 = 0.0
            do i= 1, N
                DNRM2= DNRM2 + R(i)**2
            enddo
            RESID= dsqrt(DNRM2/BNRM2)
            if ( RESID <= EPS) exit
            RHO1 = RHO

        end do




    end subroutine



end module
! gfortran -c solver.f90
