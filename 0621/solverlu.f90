module solverlu
    implicit none

    contains

    subroutine lumethod(nconst,dim,kmat,fmat,umat)

        real(8), allocatable :: fmat(:),kmat(:,:),umat(:)
        integer :: i,j,k,o

        !LAPACKで連立方程式求める用の変数
        integer :: dim,lda, ldb, info, nrhs,m,n,nconst
        integer, allocatable :: ipiv(:)
        character :: trans
        real(8) :: fmatlu(nconst*2),kmatlu(nconst*2,nconst*2),testmat(2,2)
        fmatlu = 0.0d0
        kmatlu = 0.0d0

        
        ! LAPACK の本文
        trans = "N"
        m = nconst*2     !係数行列aの行数
        n = 2     !係数行列aの列数
        lda = 2   !係数行列aの配列の第1次元
        ldb = nconst*2   !ベクトルbの配列の第1次元
        nrhs = 1    !右辺の数
        
        
        kmatlu = kmat
        fmatlu = fmat
        testmat= 4.0d0
        write(*,*)"test",m,n,lda,testmat
        !call dgetrf(m, m, kmatlu, m, ipiv, info)
        call dgetrf(n, n, testmat, n, ipiv, info)
        call dgetrs(trans, m, nrhs, kmatlu, m, ipiv, fmatlu, m, info)

        umat = fmatlu
    end subroutine

end module

!gfortran -c solverlu.f90 -I/usr/local/include -llapack95 -llapack -lblas

    




