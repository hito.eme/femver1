module inputparameter
implicit none

contains
    

    subroutine input(dieta,dizeta,point,nconst,econst,dim,element,young,poisson)
        implicit none
        integer :: nconst,econst,dim,i,j,k,o
        integer,allocatable :: element(:,:)
        real(8) :: dieta(4),dizeta(4),young,poisson
        real(8),allocatable :: node(:,:),point(:,:,:)
        !節点座標node,要素ごとの接点座標point,要素に属する節点番号element
        allocate(node(nconst,dim))
        

        open(10, file = "inputetazeta.dat")
        open(20, file = "inputxy.dat")
        open(30, file = "poissonyoung.dat")
        open(40, file = "inputelement.dat")

        
    
    
        do i = 1,4
            read(10, *)dizeta(i), dieta(i)
        end do
    
        node = 0
        do i = 1, nconst
            read(20,*)k,node(i,1:dim)
            
        end do

        read(30, *)young,poisson 
        write(*,*)"young,poisson",young,poisson
        
        element = 0
        do i = 1,econst
            read(40,*) k, element(i,1:4)
        end do    
        

        do i = 1,econst
            do j = 1,4
                point(i,j,1:dim)= node(element(i,j),1:dim)
            end do
        end do
        

        write(*,*) 'node'
        do i = 1, nconst
            write(*,*) (node(i,o),o=1,dim)
        end do

        write(*,*) 'ele'
        do i = 1, econst
            write(*,*) (element(i,o),o=1,4)
        end do

        close(10)
        close(20)
        close(30)
        close(40)

    end subroutine

    subroutine xy_point(xy,point,ele,dim)
    implicit none
    real(8),allocatable ::xy(:,:),point(:,:,:)
    integer :: ele,i,j,o,dim
        do o = 1, dim
            do j = 1, 4
                xy(j,o)= point(ele,j,o)
            end do
        end do

        write(*,*)"xy"
        do j = 1, 4
            write(*, *)(xy(j,o),o=1,2)
        end do

    end subroutine
        



    end module
    ! gfortran -c inputmodule.f90