module kmatrix 
implicit none
    real(8), allocatable :: eta(:), zeta(:), kei(:), jacobian(:,:),xy(:,:),jacobianmat(:,:),dmat(:,:),allkemat(:,:,:),kmat(:,:)
    real(8), allocatable :: point(:,:,:),node(:,:) 
    real(8) :: deta, dzeta,dieta(4), dizeta(4),jacobiandet
    real(8) :: kemat1(8,8),kemat2(8,8),kemat3(8,8),kemat4(8,8),kemat(8,8),poisson,young
    integer i, j, n, m, l, o,ke,nconst,econst,ele
    integer, allocatable :: element(:,:)

    !LAPACKで連立方程式求める用の変数
    integer :: dim,lda, ldb, info, nrhs,k
    real(8), allocatable :: b(:,:),jacobianlu(:,:),bmat(:,:),bmattranspose(:,:)
    integer, allocatable :: ipiv(:)
    character :: trans

contains
    
    

    subroutine keijyou(kei, deta, dzeta)
        implicit none
        real(8) :: kei(4), deta, dzeta
        !kei(1)はN1(形状関数を示す)
        kei(1) = (1.0d0/4)*((1.0d0-dzeta)*(1.0d0-deta)) 
        kei(2) = (1.0d0/4)*((1.0d0 + dzeta)*(1.0d0 - deta))
        kei(3) = (1.0d0/4)*((1.0d0+dzeta)*(1.0d0+deta))
        kei(4) = (1.0d0/4)*((1.0d0-dzeta)*(1.0d0+deta))

    end subroutine keijyou



    subroutine etapd(eta, dzeta)
        implicit none 
        real(8) :: eta(4), dzeta 
        !eta(1)はN1をηで偏微分したものを示す．
        eta(1) = (-1.0d0/4.0d0)*(1.0d0-dzeta)
        eta(2) = (-1.0d0/4.0d0)*(1.0d0+dzeta)
        eta(3) = (1.0d0/4.0d0)*(1.0d0+dzeta)
        eta(4) = (1.0d0/4.0d0)*(1.0d0-dzeta)

    end subroutine etapd

    subroutine zetapd(zeta, deta)
        implicit none
        real(8)  :: zeta(4), deta
        !zeta(1)はN1をζで偏微分したものを示す．
        zeta(1) = (-1.0d0/4.0d0)*(1.0d0-deta)
        zeta(2) = (1.0d0/4.0d0)*(1.0d0-deta)
        zeta(3) = (1.0d0/4.0d0)*(1.0d0+deta)
        zeta(4) = (-1.0d0/4.0d0)*(1.0d0+deta)

    end subroutine zetapd

    subroutine jacobimat(jacobian,zeta,eta,jacobianmat,xy,jacobiandet)
        implicit none
        real(8) :: jacobian(n,m), zeta(4),eta(4),jacobianmat(n,l),xy(m,l),jacobiandet
        !ここどうにかカラムメジャにしたい
        
            do j = 1,m
                jacobian(1,j) = eta(j)
                jacobian(2,j) =zeta(j)
            end do

        jacobianmat = matmul(jacobian, xy)
        write(*,*)'jacobianmat',jacobianmat
        jacobiandet = (jacobianmat(1, 1) * jacobianmat(2, 2))-(jacobianmat(1, 2)*jacobianmat(2, 1))
        write(*,*)'jacobiandet',jacobiandet


        
    end subroutine jacobimat

    subroutine lapacklu(dim,m,n,lda,ldb,info,nrhs,b,ipiv,trans,k,i,j,bmat)
        
        implicit none    
        integer :: dim,lda, ldb, info, nrhs,k,m,n,i,j
        !名前変更する
        real(8), allocatable :: x(:),y(:),b(:,:),jacobianlu(:,:)
        real(8), allocatable :: bmat(:, :), aa(:,:)
        integer, allocatable :: ipiv(:)
        character :: trans
        
        i=0
        j=0
        k=0
        
        dim = 2
        ! LAPACK の本文
        trans = "N"
        m = dim     !係数行列aの行数
        n = dim     !係数行列aの列数
        lda = dim   !係数行列aの配列の第1次元
        ldb = dim   !ベクトルbの配列の第1次元
        nrhs = 1    !右辺の数
        !配列割付

        
        allocate (x(4))!n1/x格納用
        allocate (y(4))!n1/y格納用
        allocate (jacobianlu(lda, n))   !第2次元（後ろの添え字のほう)はmax(1, n)）
            

        x=0.0d0
        y=0.0d0
        
        !DGETRF(LU分解)の呼び出し
        call dgetrf(m, n, jacobianmat, lda, ipiv, info)
        
        do k= 1,4 !四角形要素の場合

            do i = 1, dim
                do j= 1, nrhs
                    b(i, j) = 0.0d0
                end do
            end do
        
            b(1,1)= zeta(k)
            b(2,1)= eta(k)
              
            !DGETRS(連立方程式求解)呼び出し
            call dgetrs(trans, n, nrhs, jacobianmat, lda, ipiv, b, ldb, info)
              
            x(k)=b(1,1)
            y(k)=b(2,1)
           
        end do

        bmat(1,1)=x(1)
        bmat(2,1)=0
        bmat(3,1)=y(1)
        bmat(1,2)=0
        bmat(2,2)=y(1)
        bmat(3,2)=x(1)

        bmat(1,3)=x(2)
        bmat(2,3)=0
        bmat(3,3)=y(2)
        bmat(1,4)=0
        bmat(2,4)=y(2)
        bmat(3,4)=x(2)

        bmat(1,5)=x(3)
        bmat(2,5)=0
        bmat(3,5)=y(3)
        bmat(1,6)=0
        bmat(2,6)=y(3)
        bmat(3,6)=x(3)

        bmat(1,7)=x(4)
        bmat(2,7)=0
        bmat(3,7)=y(4)
        bmat(1,8)=0
        bmat(2,8)=y(4)
        bmat(3,8)=x(4)

    end subroutine lapacklu



    subroutine bmatrixtranspose(bmat, bmattranspose)
        implicit none
        real(8) :: bmat(3,8),bmattranspose(8,3)

        bmattranspose = transpose(bmat)

    end subroutine bmatrixtranspose


    subroutine dmatrix(dmat,young,poisson)
        implicit none
        real(8), allocatable :: dmat(:,:)
        real(8) :: poisson,  young
        integer :: i, o
    
        dmat(1,1)=young/(1-(poisson)**2)
        dmat(2,1)=(young*poisson)/(1-(poisson)**2)
        dmat(3,1)=0

        dmat(1,2)=(young*poisson)/(1-(poisson)**2)
        dmat(2,2)=young/(1-(poisson)**2)
        dmat(3,2)=0

        dmat(1,3)=0
        dmat(2,3)=0
        dmat(3,3)=young/(2-(2*poisson))

        write(*,*)'dmat='
        do i = 1, 3
            write(*,*) (dmat(i,o),o=1,3)
        end do

    

    end subroutine 

    subroutine kematrix(kemat1,kemat2,kemat3,kemat4,kemat)
        real(8) :: kemat1(8,8),kemat11(8,3),kemat12(8,8)
        real(8) :: kemat2(8,8),kemat21(8,3),kemat22(8,8)
        real(8) :: kemat3(8,8),kemat31(8,3),kemat32(8,8)
        real(8) :: kemat4(8,8),kemat41(8,3),kemat42(8,8)
        real(8) :: kemat(8,8)



        if (ke == 1)then
            kemat11 = matmul(bmattranspose,dmat)
            kemat12 = matmul(kemat11,bmat)
            kemat1 = kemat12*jacobiandet
            write(*,*)'kemat1='
            do i = 1, 8
               write(*,*) (kemat1(i,o),o=1,8)
            end do
        end if

        if (ke == 2)then
            kemat21 = matmul(bmattranspose,dmat)
            kemat22 = matmul(kemat21,bmat)
            kemat2 = kemat22*jacobiandet
            write(*,*)'kemat2='
            do i = 1, 8
                write(*,*) (kemat2(i,o),o=1,8)
            end do
        end if

        if (ke == 3)then
            kemat31 = matmul(bmattranspose,dmat)
            kemat32 = matmul(kemat31,bmat)
            kemat3 = kemat32*jacobiandet
            write(*,*)'kemat3='
            do i = 1, 8
                write(*,*) (kemat3(i,o),o=1,8)
            end do
        end if

        if (ke == 4)then
            kemat41 = matmul(bmattranspose,dmat)
            kemat42 = matmul(kemat41,bmat)
            kemat4 = kemat42*jacobiandet
            write(*,*)'kemat4='
            do i = 1, 8
                write(*,*) (kemat4(i,o),o=1,8)
            end do

            kemat= kemat1+kemat2+kemat3+kemat4
            
            write(*,*)"kemat="
            do i = 1, 8
                write(*,*) (kemat(i,o),o=1,8)
            end do



        end if

        







    end subroutine kematrix

end module

!コンパイル時は
        ! gfortran -c kmat.f90 -I/usr/local/include -llapack95 -llapack -lblas

