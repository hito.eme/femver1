program fem
    use kmatrix
    use fmatrix
    use solver
    implicit none

    ! real(8), allocatable :: eta(:), zeta(:), kei(:), jacobian(:,:),xy(:,:),jacobianmat(:,:),dmat(:,:),allkemat(:,:,:),kmat(:,:)
    ! real(8), allocatable :: point(:,:,:),node(:,:) 
    ! real(8) :: deta, dzeta,dieta(4), dizeta(4),jacobiandet
    ! real(8) :: kemat1(8,8),kemat2(8,8),kemat3(8,8),kemat4(8,8),kemat(8,8),poisson,young
    ! integer i, j, n, m, l, o,ke,nconst,econst,ele
    ! integer, allocatable :: element(:,:)

    ! !LAPACKで連立方程式求める用の変数
    ! integer :: dim,lda, ldb, info, nrhs,k
    ! real(8), allocatable :: b(:,:),jacobianlu(:,:),bmat(:,:),bmattranspose(:,:)
    ! integer, allocatable :: ipiv(:)
    ! character :: trans

    n = 2
    m = 4
    l = 2
    dim=2
    ldb = dim   !ベクトルbの配列の第1次元
    nrhs = 1    !右辺の数
    nconst = 6  !節点数
    econst = 2  !要素数

    open(10, file = "inputetazeta.dat")
    open(20, file = "inputxy.dat")
    open(30, file = "poissonyoung.dat")
    open(40, file = "inputelement.dat")
    allocate(eta(4))
    allocate(zeta(4))
    allocate(kei(4))
    allocate(jacobian(n,m))
    allocate(xy(m,l))
    allocate(jacobianmat(n,l))
    allocate(jacobianlu(n,l))
    allocate(b(ldb, nrhs))
    allocate(ipiv(n))     !次元はmax(1, min(m, n))
    allocate(bmat(3,8))
    allocate(bmattranspose(8,3))
    allocate(dmat(3,3))
    !節点座標node,要素ごとの接点座標point,要素に属する節点番号element
    allocate(node(nconst,dim))
    !要素，要素の節点数(2次元は4),xとyが格納される
    allocate(point(econst,4,dim))
    allocate(element(nconst,4))
    allocate(allkemat(nconst,8,8))
    allkemat=0.0d0
    allocate(kmat(nconst*2,nconst*2))
    kmat=0.0d0
    allocate(fmat(nconst*2))
    !solver.f90の割付
    allocate(R(nconst*2))
    allocate(X(nconst*2))
    allocate(P(nconst*2))
    allocate(Q(nconst*2))



    
    read(30, *)young,poisson
    
    write(*,*)"young,poisson",young,poisson
    close(30)
    

    do i = 1,4
        read(10, *)dieta(i), dizeta(i)
    end do
    
    !todo:ここから　subroutine化したい 5/14
    !要素と節点と座標を読み込むところ
    
    call dmatrix(dmat,young,poisson)
    node = 0
    do i = 1, nconst
        read(20,*)k,node(i,1:dim)
    end do

    element = 0
    do i = 1,econst
        read(40,*) k, element(i,1:4)
    end do    
    
    do i = 1,econst
        do j = 1,4
            point(i,j,1:dim)= node(element(i,j),1:dim)
        end do
    end do
    close(20)

    write(*,*) 'node'
    do i = 1, nconst
        write(*,*) (node(i,o),o=1,dim)
    end do

    write(*,*) 'ele'
    do i = 1, econst
        write(*,*) (element(i,o),o=1,4)
    end do

    
    !ここまで

    do ele = 1,econst
        do o = 1, l
            do j = 1, m
                xy(j,o)= point(ele,j,o)
            end do
        end do

            write(*,*)"xy"
            !TOdo: 本当はdo j = 1, mにしたい5/14
            do j = 1, 4
                write(*, *)(xy(j,o),o=1,2)
            end do
        

    
        do  ke = 1,4 !積分点数が2の場合
            write(*,*)""
            deta  = dieta(ke)
            dzeta = dizeta(ke)

            write(*,*) 'eta=', deta
            write(*,*) 'zeta=', dzeta 
        
        

            call keijyou(kei, deta, dzeta)
            call etapd(eta,dzeta)
            call zetapd(zeta,deta)
            call jacobimat(jacobian,eta,zeta,jacobianmat,xy,jacobiandet)
            call lapacklu(dim,m,n,lda,ldb,info,nrhs,b,ipiv,trans,k,i,j,bmat)
            write(*,*)'bmat='
            do i = 1, 3
                write(*,*) (bmat(i,o),o=1,8)
            end do
            call bmatrixtranspose(bmat, bmattranspose)
            write(*,*)'bmattranspose='
            do i = 1, 8
                write(*,*) (bmattranspose(i,o),o=1,3)
            end do
            
            call kematrix(kemat1,kemat2,kemat3,kemat4,kemat)
        end do

        !todo:ここからサブルーチン化5/17
        do i=1,8
            do j=1,8
                allkemat(ele,i,j)=kemat(i,j)
            end do
        end do 

        write(*,*)"allkemat"
        do i = 1, 8
                write(*,*) (allkemat(ele,i,o),o=1,8)
        end do

        !todo: エラー多発!!数式確認必要5/17 
        do i= 1,4
            do j = 1,4
                kmat(2*element(ele,i)-1,2*element(ele,j)-1:2*element(ele,j))=&
                &kmat(((2*element(ele,i))-1),2*element(ele,j)-1:2*element(ele,j))+allkemat(ele,2*i-1,2*j-1:2*j)

                kmat(2*element(ele,i)  ,2*element(ele,j)-1:2*element(ele,j))=&
                &kmat((2*element(ele,i)    ),2*element(ele,j)-1:2*element(ele,j))+allkemat(ele,2*i  ,2*j-1:2*j)
                
            end do
        end do 
        !ここまで

    end do
    write(*,*)"kmat"
    do i = 1, nconst*2
        write(*,*) (kmat(i,o),o=1,nconst*2)
    end do
    write(*,*)'nconst',nconst
    call inputload(fmat)

    call cgmethod(X,RESID)



end program

! gfortran fem_v1.f90 kmat.o fmat.o solver.o -I/usr/local/include -llapack95 -llapack -lblas